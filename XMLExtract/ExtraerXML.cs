﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace XMLExtract
{
    class ExtraerXML
    {
        private String ruta;

        public ExtraerXML(string ruta)
        {
            this.ruta = ruta;
        }

        public void leerXML()
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(ruta);
            XmlNodeList comprobantes = xml.GetElementsByTagName("cfdi:Comprobante");
            XmlElement g = (XmlElement)comprobantes.Item(0);
            GeneralesDto generalesdto = new GeneralesDto();
            generalesdto.folio = g.GetAttribute("Folio");
            generalesdto.serie = g.GetAttribute("Serie");
            generalesdto.fecha = g.GetAttribute("Fecha");
            generalesdto.subtotal = g.GetAttribute("SubTotal");
            generalesdto.total = g.GetAttribute("Total");
            generalesdto.lugarexpedicion = g.GetAttribute("LugarExpedicion");
            generalesdto.moneda = g.GetAttribute("Moneda");
            generalesdto.tipocambio = g.GetAttribute("TipoCambio");
            generalesdto.tipocomprobante = g.GetAttribute("TipoDeComprobante");
            generalesdto.formapago = g.GetAttribute("FormaPago");
            generalesdto.metodopago = g.GetAttribute("MetodoPago");

            XmlNodeList  emisor= xml.GetElementsByTagName("cfdi:Emisor");   
            XmlElement e = (XmlElement)emisor.Item(0);
            EmisorDto emisordto = new EmisorDto();
            emisordto.nombre = e.GetAttribute("Nombre");
            emisordto.rfc = e.GetAttribute("Rfc");
            emisordto.regimenfiscal = e.GetAttribute("RegimenFiscal");

            XmlNodeList receptor = xml.GetElementsByTagName("cfdi:Receptor");
            XmlElement r = (XmlElement)receptor.Item(0);
            ReceptorDto receptordto = new ReceptorDto();
            receptordto.nombre = r.GetAttribute("Nombre");
            receptordto.rfc = r.GetAttribute("Rfc");
            receptordto.usoCfdi = r.GetAttribute("UsoCFDI");

            XmlNodeList timbrefiscal = xml.GetElementsByTagName("tfd:TimbreFiscalDigital");
            XmlElement t = (XmlElement)timbrefiscal.Item(0);
            TimbradoDto timbradoDto = new TimbradoDto();
            timbradoDto.UUID = t.GetAttribute("UUID");
            timbradoDto.fecha = t.GetAttribute("FechaTimbrado");
           //Console.WriteLine(timbrefiscal.Count);
            /* aux = xml.GetElementsByTagName("cfdi:Impuestos");
             XmlNodeList lista = aux[1].ChildNodes;
             /*lista = lista[0].ChildNodes;
             lista = lista[0].ChildNodes;
             lista = lista[0].ChildNodes;*/
            //  Console.WriteLine(lista[0].Name);


        }
    }
}

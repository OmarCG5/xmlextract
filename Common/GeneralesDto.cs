﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
   public  class GeneralesDto
    {
        public String folio { get; set; }
        public String formapago { get; set; }
        public String serie { get; set; }
        public String lugarexpedicion { get; set; }
        public String moneda { get; set; }
        public String subtotal { get; set; }
        public String tipocomprobante { get; set; }
        public String total { get; set; }
        public String tipocambio { get; set; }
        public String fecha { get; set; }
        public String metodopago { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class EmisorDto
    {
        public String nombre { get; set; }
        public String rfc { get; set; }
        public String regimenfiscal { get; set; }
    }
}
